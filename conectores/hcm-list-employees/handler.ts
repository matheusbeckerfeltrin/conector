import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const PATH = '/hcm/employeejourney/entities/employee';

const hcmListEmployees = async (event) => {

  const url = event.headers['x-platform-environment'] + PATH;

  const filter = [] as string[];

  const contractType = event?.queryStringParameters?.contractType;
  const hireDate = event?.queryStringParameters?.hireDate;
  const dismissalDate = event?.queryStringParameters?.dismissalDate;
  const employeeType = event?.queryStringParameters?.employeeType;
  const registerNumber = event?.queryStringParameters?.registerNumber;
  const employerId = event?.queryStringParameters?.employerId;
  const employerNumemp = event?.queryStringParameters?.employerNumemp;
  const employerTradingName = event?.queryStringParameters?.employerTradingName;
  const personFullname = event?.queryStringParameters?.personFullname;

  if (contractType) {
    filter.push(`containing(upper(contractType), upper('${contractType}'))`);
  }

  if (hireDate) {
    filter.push(`containing(upper(hireDate), upper('${hireDate}'))`);
  }

  if (dismissalDate) {
    filter.push(`dismissalDate eq '${dismissalDate}'`);
  }

  if (employeeType) {
    filter.push(`containing(upper(employeeType), upper('${employeeType}'))`);
  }

  if (registerNumber) {
    filter.push(`registerNumber eq '${registerNumber}'`);
  }

  if (employerId) {
    filter.push(`employerId eq '${employerId}'`);
  }

  if (employerNumemp) {
    filter.push(`employerNumemp eq '${employerNumemp}'`);
  }

  if (employerTradingName) {
    filter.push(`containing(upper(employerTradingName), upper('${employerTradingName}'))`);
  }

  if (personFullname) {
    filter.push(`containing(upper(personFullname), upper('${personFullname}'))`);
  }

  const page = event?.queryStringParameters?.page || 0;
  const size = event?.queryStringParameters?.size || 10;

  try {
    const responseData = await Axios.get(`${url}${filter.length ? '?filter=' : ''}${filter.length ? filter.join(' AND ') : ''}${filter.length ? '&' : '?'}size=${size}&offset=${page}`, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    const data = responseData.data;
    const contents: any[] = [];
    const response = {
      contents: contents,
      totalPages: data.totalPages,
      totalElements: data.totalElements
    }

    data.contents.forEach(employee => {
      const employeeFormated = {
        id: employee.id,
        contractType: employee.contractType,
        hireDate: employee.hireDate,
        dismissalDate: employee.dismissalDate,
        employeeType: employee.employeeType,
        registerNumber: employee.registerNumber,
        employerId: employee.employer.id,
        employerNumemp: employee.employer.numemp,
        employerTradingName: employee.employer.tradingName,
        employerCompanyName: employee.employer.companyName,
        employerCnpj: employee.employer.cnpj,
        personId: employee.person.id,
        personFullname: employee.person.fullName,
        personCpf: employee.person.cpf
      };
      
      response.contents.push(employeeFormated);
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(hcmListEmployees);