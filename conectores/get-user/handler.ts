import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const PATH = '/platform/user/queries/getUser'
let response;

const getUser = async (event) => {

  try {

    const url = event.headers['x-platform-environment'] + PATH;

    event.body.businessData = event.body.businessData ? JSON.parse(event.body.businessData) : {};

    const responseData = await Axios.post(url, event.body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    response = {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseData?.data)
    };

  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }

  return response;
};

export const main = middyfy(getUser);