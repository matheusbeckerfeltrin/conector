import { middyfy } from '../../libs/lambda';

const handler = async (event) => {

  // Toda sua lógica e código deve estar aqui

  return {
    statusCode: 200,
    body: JSON.stringify({seu: 'retorno'})
  };

};

export const main = middyfy(handler);